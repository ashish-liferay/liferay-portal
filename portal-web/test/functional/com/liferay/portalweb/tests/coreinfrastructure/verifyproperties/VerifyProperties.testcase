@component-name = "portal-verify-properties"
definition {

	property app.server.types = "tomcat";
	property ci.retries.disabled = "true";
	property database.types = "mysql";
	property portal.release = "true";
	property portal.upstream = "true";
	property testray.main.component.name = "Verify Properties";

	setUp {
		TestCase.setUpPortalInstance();
	}

	@description = "LRQA-78904: Upgrade will be stopped if Verify Properties fails when enable AutoUpgrade"
	@priority = 3
	test UpgradeStopsIfVerifyPropertiesFail {
		property custom.properties = "module.framework.runtime.start.level=20${line.separator}upgrade.database.auto.run=true";
		property data.archive.type = "data-archive-portal";
		property portal.version = "7.4.13";
		property skip.get.testcase.database.properties = "true";
		property skip.start.app.server = "true";
		property skip.upgrade-legacy-database = "true";
		property test.assert.warning.exceptions = "false";
		property test.name.skip.portal.instance = "VerifyProperties#UpgradeStopsIfVerifyPropertiesFail";

		Portlet.startServer(deleteLiferayHome = "false");

		AntCommand(
			locator1 = "build-test.xml",
			value1 = "wait-for-server-shutdown -Dapp.server.port.number=8080");

		AssertConsoleTextPresent(value1 = "Completed com.liferay.portal.verify.VerifyProperties#verifySystemProperties");

		AssertConsoleTextPresent(value1 = "Completed com.liferay.portal.verify.VerifyProperties#verifyPortalProperties");

		AssertConsoleTextNotPresent(value1 = "Upgrading com.liferay.portal.upgrade.PortalUpgradeProcess");

		AssertConsoleTextPresent(value1 = "Stopping the server due to incorrect use of migrated portal properties");
	}

	@description = "LRQA-78467: Verify Properties is executed before the PortalUpgradeProcess starts to upgrade database when enable AutoUpgrade on startup"
	@priority = 3
	test VerifyPropertiesIsExecutedUsingAutoUpgrade {
		property custom.properties = "upgrade.database.auto.run=true";
		property data.archive.type = "data-archive-portal";
		property portal.version = "7.4.13";
		property skip.get.testcase.database.properties = "true";
		property skip.upgrade-legacy-database = "true";
		property test.name.skip.portal.instance = "VerifyProperties#VerifyPropertiesIsExecutedUsingAutoUpgrade";

		AssertConsoleTextPresent(value1 = "Completed com.liferay.portal.verify.VerifyProperties#verifySystemProperties");

		AssertConsoleTextPresent(value1 = "Completed com.liferay.portal.verify.VerifyProperties#verifyPortalProperties");

		AssertConsoleTextPresent(value1 = "Upgrading com.liferay.portal.upgrade.PortalUpgradeProcess");
	}

	@description = "LRQA-78466: Verify Properties is executed before the PortalUpgradeProcess starts to upgrade database when running upgrade tool"
	@priority = 4
	test VerifyPropertiesIsOnlyExecutedInUpgradeProcess {
		property data.archive.type = "data-archive-portal";
		property portal.version = "7.4.13";
		property test.name.skip.portal.instance = "VerifyProperties#VerifyPropertiesIsOnlyExecutedInUpgradeProcess";

		var liferayHome = PropsUtil.get("liferay.home.dir.name");

		var upgradeLogContent = FileUtil.read("${liferayHome}/tools/portal-tools-db-upgrade-client/logs/upgrade.log");
		var upgradeOnlyLogging = "Completed com.liferay.portal.verify.VerifyProperties#verifyPortalProperties,Completed com.liferay.portal.verify.VerifyProperties#verifySystemProperties,Upgrading com.liferay.portal.upgrade.PortalUpgradeProcess";

		for (var expectedLogString : list ${upgradeOnlyLogging}) {
			if (!(contains(${upgradeLogContent}, ${expectedLogString}))) {
				fail("${expectedLogString} is not found in upgrade.log.");
			}

			AssertConsoleTextPresent(value1 = ${expectedLogString});
		}

		Portlet.shutdownServer();

		Portlet.startServer(keepOsGiState = "true");

		for (var unexpectedLogString : list ${upgradeOnlyLogging}) {
			AssertConsoleTextNotPresent(value1 = ${unexpectedLogString});
		}
	}

	@description = "LRQA-78468: Verify Properties not run when start portal with empty database"
	@priority = 3
	test VerifyPropertiesNotRunWithEmptyDatabase {
		property test.name.skip.portal.instance = "VerifyProperties#VerifyPropertiesNotRunWithEmptyDatabase";

		AssertConsoleTextNotPresent(value1 = "Completed com.liferay.portal.verify.VerifyProperties#verifySystemProperties");

		AssertConsoleTextNotPresent(value1 = "Completed com.liferay.portal.verify.VerifyProperties#verifyPortalProperties");
	}

}
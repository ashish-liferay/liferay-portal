@component-name = "portal-segmentation"
definition {

	property portal.release = "true";
	property portal.upstream = "true";
	property testray.main.component.name = "Segmentation";

	setUp {
		TestCase.setUpPortalInstance();

		User.firstLoginPG();

		JSONGroup.addGroup(groupName = "Test Site Name");

		Navigator.openURL();

		task ("Add role with permissions") {
			JSONRole.addRegularRole(
				roleKey = "Roles Regrole Name",
				roleTitle = "Roles Regrole Name");

			Permissions.definePermissionViaJSONAPI(
				resourceAction = "ACCESS_IN_CONTROL_PANEL",
				resourceName = "com_liferay_content_dashboard_web_portlet_ContentDashboardAdminPortlet",
				roleTitle = "Roles Regrole Name");

			Permissions.definePermissionViaJSONAPI(
				resourceAction = "VIEW_CONTROL_PANEL",
				resourceName = 90,
				roleTitle = "Roles Regrole Name");
		}
	}

	tearDown {
		var testPortalInstance = PropsUtil.get("test.portal.instance");

		if (${testPortalInstance} == "true") {
			PortalInstances.tearDownCP();
		}
		else {
			JSONUser.tearDownNonAdminUsers();

			JSONRole.deleteRole(roleTitle = "Roles Regrole Name");

			JSONGroup.deleteGroupByName(groupName = "Test Site Name");
		}
	}

	@description = "LPS-130918 Given the Segment Service Configuration in Instance Settings And Enable Segmentation by roles is checked in System Settings When Enable Segmentation by roles is disabled And the user accesses the Segments tab in the role assignment screen Then the user may still add segments to the list by using the ""+"" button, but assigning won't be accomplished"
	@priority = 4
	test AssigningBySegmentNotWorkingDisabledInstanceSettings {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#AssigningBySegmentNotWorkingDisabledInstanceSettings";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("Add segment for the user created") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Global",
				operator = "contains",
				segmentName = "Segment global usersn",
				text = "usersn");
		}

		task ("Enable Assign roles by segment from system settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Disable segmentation by roles from instance settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "Instance Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "Virtual Instance Scope");

			Segmentation.disableAssingRolesBySegmentsCheckbox();
		}

		task ("Assign the segment to the role created") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role.addAssignee(
				assigneeName = "Segment global usersn",
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Log out and login with the user assigned to the segment") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the role is not working") {
			AssertElementNotPresent(locator1 = "ApplicationsMenu#PORTLET");
		}

		task ("Login with Test user to allow teardown") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "test@liferay.com",
				userLoginFullName = "Test Test");
		}

		task ("Delete one of the segments") {
			Segmentation.openSegmentsAdmin(siteURLKey = "global");

			Segmentation.deleteSegment(entry = "Segment global usersn");
		}
	}

	@description = "LPS-122800: Validate if, with the option Enable Assign roles by segments disabled, the option Assign Site Roles is disabled"
	@priority = 4
	@uitest
	test AssignSiteRolesOptionInSegmentsEditor {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#AssignSiteRolesOptionInSegmentsEditor";

		task ("Add segment in Test Site Name") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Test Site Name",
				operator = "contains",
				segmentName = "Segment site usersn",
				text = "usersn");
		}

		task ("Go to segments portlet") {
			Segmentation.openSegmentsAdmin(siteURLKey = "test-site-name");
		}

		task ("Open segment's options and assert the option Assign site roles is disabled") {
			Segmentation.openSegmentOptions(segmentName = "site");

			AssertElementPresent(
				key_text = "",
				locator1 = "Dropdown#ANY_DISABLED");
		}

		task ("Enable Assign roles by segment") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Go to segments portlet") {
			Segmentation.openSegmentsAdmin(siteURLKey = "test-site-name");
		}

		task ("Open segment's options and assert the option Assign site roles is enabled") {
			Segmentation.openSegmentOptions(segmentName = "site");

			AssertElementPresent(
				key_text = "Assign Site Roles",
				locator1 = "Dropdown#ANY_ENABLED");
		}
	}

	@description = "LPS-130918 Given the Segment Service Configuration in Instance Settings And Enable Segmentation by roles is checked in System Settings When Enable Segmentation by roles is enabled And the user accesses the Segments tab in the role assignment screen Then no warning message is shown and the assigning will be accomplished"
	@priority = 3
	test CanAssignBySegmentEnabledInstanceSettings {
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#CanAssignBySegmentEnabledInstanceSettings";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("Add segment for the user created") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Global",
				operator = "contains",
				segmentName = "Segment global usersn",
				text = "usersn");
		}

		task ("Enable Assign roles by segment from system settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Enable segmentation by roles from instance settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "Instance Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "Virtual Instance Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Assign the segment to the role created") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role.addAssignee(
				assigneeName = "Segment global usersn",
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Log out and login with the user assigned to the segment") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the role is working") {
			ApplicationsMenuHelper.openApplicationsMenu();

			ApplicationsMenu.gotoPanel(panel = "Applications");

			AssertElementPresent(
				key_category = "Content",
				key_portlet = "Content Dashboard",
				locator1 = "ApplicationsMenu#PORTLET");
		}

		task ("Login with Test user to allow teardown") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "test@liferay.com",
				userLoginFullName = "Test Test");
		}

		task ("Delete one of the segments") {
			Segmentation.openSegmentsAdmin(siteURLKey = "global");

			Segmentation.deleteSegment(entry = "Segment global usersn");
		}
	}

	@description = "LPS-130918 Given the Segment Service Configuration in Instance Settings And Enable Segmentation by roles is checked in System Settings When Enable Segmentation by roles is enabled Then it shows a dialog allowing user to assign/unassign site roles"
	@priority = 4
	test CanAssignSiteRolesToSegment {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#CanAssignSiteRolesToSegment";

		task ("Add segment in Test Site Name") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Test Site Name",
				operator = "contains",
				segmentName = "Segment site usersn",
				text = "usersn");
		}

		task ("Enable Assign roles by segment at System Settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Disable segmentation by roles from instance settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "Instance Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "Virtual Instance Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Go to segments portlet") {
			Segmentation.openSegmentsAdmin(siteURLKey = "test-site-name");
		}

		task ("Add site role to segment") {
			Segmentation.assignSiteRoles(
				roleName = "Site Member",
				segmentName = "Segment site usersn");
		}
	}

	@description = "LPS-130918 Given the Segment Service Configuration in Instance Settings When Enable Segmentation by roles is disabled And the user accesses the Segments tab in the role assignment screen Then a message will show: ""Warning: Assigning roles by segment is disabled. To enable, go to System Settings > Segments > Segments Service."""
	@priority = 3
	test CheckMessageSegmentationByRolesDisabledInstanceSettings {
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#CheckMessageSegmentationByRolesDisabledInstanceSettings";

		task ("Disable segmentation by roles from instance settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "Instance Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "Virtual Instance Scope");

			Segmentation.disableAssingRolesBySegmentsCheckbox();
		}

		task ("Go to the role segments assignee's section") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role._goToRoleAssigneeType(
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Assert the warning message is displayed") {
			AssertTextPresent(
				locator1 = "Segmentation#ASSIGN_SITE_ROLES_DISABLED_WARNING",
				textValue = "Assigning roles by segment is disabled",
				value1 = "Assigning roles by segment is disabled. To enable, go to Instance Settings.");
		}
	}

	@description = "LPS-121943: Validate if, with the option Enable Assign roles by segments disabled, the role is not working for the user"
	@priority = 4
	@uitest
	test DisableAssignRolesBySegment {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#DisableAssignRolesBySegment";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				newPassword = "test",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("Add segment for the user created") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Global",
				operator = "contains",
				segmentName = "Segment global usersn",
				text = "usersn");
		}

		task ("Assign the segment to the role created") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role.addAssignee(
				assigneeName = "Segment global usersn",
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Assert the Enable Assign roles by segments checkbox is disabled") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.disableAssingRolesBySegmentsCheckbox();
		}

		task ("Log out and login with the user assigned to the segment") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the role is not working") {
			AssertElementNotPresent(locator1 = "ApplicationsMenu#PORTLET");
		}

		task ("Login with Test user to allow teardown") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "test@liferay.com",
				userLoginFullName = "Test Test");
		}

		task ("Delete one of the segments") {
			Segmentation.openSegmentsAdmin(siteURLKey = "global");

			Segmentation.deleteSegment(entry = "Segment global usersn");
		}
	}

	@description = "LPS-122799: Validate if, with the option Enable Assign roles by segments disabled, the warning message is displayed"
	@priority = 3
	@uitest
	test DisableAssignRolesWarningMessage {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#DisableAssignRolesWarningMessage";

		task ("Assert the Enable Assign roles by segments checkbox is disabled") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.disableAssingRolesBySegmentsCheckbox();
		}

		task ("Go to the role segments assignee's section") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role._goToRoleAssigneeType(
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Assert the warning message is displayed") {
			AssertTextPresent(
				locator1 = "Segmentation#ASSIGN_SITE_ROLES_DISABLED_WARNING",
				textValue = "Assigning roles by segment is disabled",
				value1 = "Assigning roles by segment is disabled. To enable, go to Instance Settings.");
		}
	}

	@description = "LPS-130918 Given the Segment Service Configuration in Instance Settings And Enable Segmentation by roles is checked in System Settings When Enable Segmentation by roles is disabled Then the Assign Site Roles option will be disabled in the Segments Editor"
	@priority = 4
	test DisabledAssignSiteRolesOptionInSegmentsEditor {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#DisabledAssignSiteRolesOptionInSegmentsEditor";

		task ("Add segment in Test Site Name") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Test Site Name",
				operator = "contains",
				segmentName = "Segment site usersn",
				text = "usersn");
		}

		task ("Enable Assign roles by segment at System Settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Disable segmentation by roles from instance settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "Instance Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "Virtual Instance Scope");

			Segmentation.disableAssingRolesBySegmentsCheckbox();
		}

		task ("Go to segments portlet") {
			Segmentation.openSegmentsAdmin(siteURLKey = "test-site-name");
		}

		task ("Open segment's options and assert the option Assign site roles is enabled") {
			Segmentation.openSegmentOptions(segmentName = "site");

			AssertElementPresent(
				key_text = "Assign Site Roles",
				locator1 = "Dropdown#ANY_DISABLED");
		}
	}

	@description = "LPS-161227 Bug: NoSuchExperienceException when attempting to edit Content Page with Enable Assign Roles by Segment configuration enabled"
	@priority = 5
	test EditPageAfterEnableAssignRole {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#";

		task ("Create a Content Page") {
			JSONLayout.addPublicLayout(
				groupName = "Test Site Name",
				layoutName = "Content Page",
				type = "content");
		}

		task ("Enable Assign roles by segment from system settings") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Navigate to page editor") {
			ContentPagesNavigator.openEditContentPage(
				pageName = "Content Page",
				siteName = "Test Site Name");
		}

		task ("Check no error are thrown ") {
			AssertConsoleTextNotPresent(value1 = "javax.servlet.ServletException");
		}
	}

	@description = "LPS-121943: Validate if, with the option Enable Assign roles by segments enabled, the role is working for the user."
	@priority = 4
	@uitest
	test EnableAssignRolesBySegment {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#EnableAssignRolesBySegment";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("Add segment for the user created") {
			JSONSegmentsentry.addSegment(
				fieldName = "Screen Name",
				groupName = "Global",
				operator = "contains",
				segmentName = "Segment global usersn",
				text = "usersn");
		}

		task ("Assign the segment to the role created") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role.addAssignee(
				assigneeName = "Segment global usersn",
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Enable Assign roles by segment") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Log out and login with the user assigned to the segment") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the role is working") {
			ApplicationsMenuHelper.openApplicationsMenu();

			ApplicationsMenu.gotoPanel(panel = "Applications");

			AssertElementPresent(
				key_category = "Content",
				key_portlet = "Content Dashboard",
				locator1 = "ApplicationsMenu#PORTLET");
		}

		task ("Login with Test user to allow teardown") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "test@liferay.com",
				userLoginFullName = "Test Test");
		}

		task ("Delete one of the segments") {
			Segmentation.openSegmentsAdmin(siteURLKey = "global");

			Segmentation.deleteSegment(entry = "Segment global usersn");
		}
	}

	@description = "LPS-122799: Validate if, with the option Enable Assign roles by segments enabled, the warning message is not displayed"
	@priority = 3
	@uitest
	test EnableAssignRolesNoWarningMessage {
		property portal.acceptance = "true";
		property test.name.skip.portal.instance = "SegmentationAssigningRoles#EnableAssignRolesNoWarningMessage";

		task ("Enable Assign roles by segment") {
			ApplicationsMenu.gotoPortlet(
				category = "Configuration",
				panel = "Control Panel",
				portlet = "System Settings");

			SystemSettings.gotoConfiguration(
				configurationCategory = "Segments",
				configurationName = "Segments Service",
				configurationScope = "System Scope");

			Segmentation.enableAssingRolesBySegmentsCheckbox();
		}

		task ("Go to the role segments assignee's section") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Roles");

			Role._goToRoleAssigneeType(
				assigneeType = "Segments",
				roleTitle = "Roles Regrole Name");
		}

		task ("Assert the warning message is NOT displayed") {
			AssertTextNotPresent(
				locator1 = "Segmentation#ASSIGN_SITE_ROLES_DISABLED_WARNING",
				textValue = "Assigning roles by segment is disabled",
				value1 = "Assigning roles by segment is disabled. To enable, go to Instance Settings.");
		}
	}

	@description = "LPS-167023 Given the Roles portlet When the user tries to add a new role And the user wants to add segments permissions Then the segments permissions are allocated in Control Panel > Users"
	@priority = 4
	test SegmentsPermissionsAllocatedInControlPanel {
		property custom.properties = "feature.flag.LPS-166954=true";
		property portal.acceptance = "true";

		task ("Create a new role through Roles Admin Portlet") {
			Role.openRolesAdmin();

			Navigator.gotoNavTab(navTab = "Regular Roles");

			Role.add(roleTitle = "SegmentsPermissionsAllocatedInControlPanel Role");
		}

		task ("Assert the segments permissions are allocated in Control Panel > Users") {
			Navigator.gotoNavItem(navItem = "Define Permissions");

			Click.clickAt(
				key_navItem = "Users",
				locator1 = "RolesPermissionsNavigation#PERMISSIONS_NAVITEM_COLLAPSED");

			AssertElementPresent(
				key_permissionNavHeader = "Control Panel",
				key_permissionNavItem = "Segments",
				locator1 = "RolesPermissionsNavigation#PERMISSIONS_NAVIGATION");
		}
	}

	@description = "LPS-167023 Given the user creates a new role When the user clicks on Control Panel > Users > Segments Then in General Permissions, the check Access in Control Panel permission is displayed"
	@priority = 4
	test SegmentsPermissionsHaveAccessToControlPanel {
		property custom.properties = "feature.flag.LPS-166954=true";
		property portal.acceptance = "true";

		task ("Create a new role through Roles Admin Portlet") {
			Role.openRolesAdmin();

			Navigator.gotoNavTab(navTab = "Regular Roles");

			Role.add(roleTitle = "SegmentsPermissionsHaveAccessToControlPanel Role");
		}

		task ("Define permissions and check Access in Control Panel permission is displayed") {
			Role.editPermissionCP(
				define = "true",
				permissionNavHeader = "Users",
				permissionNavItem = "Segments",
				permissionSheetSubtitle = "Application Permissions",
				permissionValues = "Access in Control Panel");

			Role.viewPermissionCP(
				permissionDefinitionValue = "View Control Panel Menu",
				permissionNavigationValue = "Portal");
		}
	}

	@description = "LPS-167023 Given the Roles portlet When the user tries to add a new role And the user wants to add segments permissions Then the segments permissions are not allocated in Site and Asset Library Administration > People"
	@priority = 3
	test SegmentsPermissionsNotAllocatedInSiteAdministration {
		property custom.properties = "feature.flag.LPS-166954=true";

		task ("Create a new role through Roles Admin Portlet") {
			Role.openRolesAdmin();

			Navigator.gotoNavTab(navTab = "Regular Roles");

			Role.add(roleTitle = "SegmentsPermissionsNotAllocatedInSiteAdministration Role");
		}

		task ("Assert the segments permissions are not allocated in Site and Asset Library Administration > People") {
			Navigator.gotoNavItem(navItem = "Define Permissions");

			WaitForElementNotPresent(locator1 = "Message#SUCCESS_DISMISSIBLE");

			ScrollWebElementIntoView(
				key_navItem = "Site and Asset Library Administration",
				locator1 = "RolesPermissionsNavigation#PERMISSIONS_NAVITEM_COLLAPSED");

			Click.pauseClickAt(
				key_navItem = "Site and Asset Library Administration",
				locator1 = "RolesPermissionsNavigation#PERMISSIONS_NAVITEM_COLLAPSED",
				value1 = "Site and Asset Library Administration");

			Click.clickAt(
				key_navItem = "People",
				locator1 = "RolesPermissionsNavigation#PERMISSIONS_NAVITEM_COLLAPSED");

			AssertElementNotPresent(
				key_permissionNavHeader = "Site and Asset Library Administration",
				key_permissionNavItem = "Segments",
				locator1 = "RolesPermissionsNavigation#PERMISSIONS_NAVIGATION");
		}
	}

	@description = "LPS-167023 Given a user without Segments permissions When the user clicks on Applications Menu > Control Panel Then the Segments portlet is not present and the user cannot access to the Segments page"
	@priority = 5
	test UserWithoutPermissionsCannotAccessSegmentsPortlet {
		property custom.properties = "feature.flag.LPS-166954=true";
		property portal.acceptance = "true";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("When Assign the user with regular role") {
			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the Segments portlet is not present and the user cannot access to the Segments page") {
			AssertElementNotPresent(locator1 = "ApplicationsMenu#TOGGLE");

			Segmentation.openSegmentsAdmin(siteURLKey = "test-site-name");

			AssertTextEquals(
				locator1 = "Portlet#ERROR",
				value1 = "You do not have the roles required to access this portlet.");
		}
	}

	@description = "LPS-167023 Given a user with Segments permissions And with View Control Panel Menu permission When the user clicks on Applications Menu > Control Panel > Segment Then the user can access to the Segments page"
	@priority = 5
	test UserWithPermissionsCanAccessSegmentsPortlet {
		property custom.properties = "feature.flag.LPS-166954=true";
		property portal.acceptance = "true";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("Create a new role through Roles Admin Portlet") {
			JSONRole.addRegularRole(roleTitle = "UserWithPermissionsCanAccessSegmentsPortlet Role");
		}

		task ("Define permissions and check Access in Control Panel permission is displayed") {
			Permissions.definePermissionViaJSONAPI(
				resourceAction = "VIEW_CONTROL_PANEL",
				resourceName = 90,
				roleTitle = "UserWithPermissionsCanAccessSegmentsPortlet Role",
				roleType = "regular");

			Permissions.definePermissionViaJSONAPI(
				resourceAction = "PERMISSIONS",
				resourceName = "com_liferay_segments_web_internal_portlet_SegmentsPortlet",
				roleTitle = "UserWithPermissionsCanAccessSegmentsPortlet Role",
				roleType = "regular");

			Permissions.definePermissionViaJSONAPI(
				resourceAction = "ACCESS_IN_CONTROL_PANEL",
				resourceName = "com_liferay_segments_web_internal_portlet_SegmentsPortlet",
				roleTitle = "UserWithPermissionsCanAccessSegmentsPortlet Role",
				roleType = "regular");

			Permissions.definePermissionViaJSONAPI(
				resourceAction = "VIEW",
				resourceName = "com.liferay.segments",
				roleTitle = "UserWithPermissionsCanAccessSegmentsPortlet Role",
				roleType = "regular");
		}

		task ("When Assign the user with regular role") {
			JSONRole.assignRoleToUser(
				roleTitle = "UserWithPermissionsCanAccessSegmentsPortlet Role",
				userEmailAddress = "userea@liferay.com");

			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the user can access to the Segments page") {
			ApplicationsMenu.gotoPortlet(
				category = "Users",
				panel = "Control Panel",
				portlet = "Segments");

			AssertTextEquals.assertPartialText(
				locator1 = "Message#EMPTY_INFO",
				value1 = "There are no segments.");
		}
	}

	@description = "LPS-167023 Given a user with Segments permissions And without View Control Panel Menu permission When the user clicks on Applications Menu > Control Panel Then the Segments portlet is not present and the user cannot access to the Segments page"
	@priority = 5
	test UserWithSegmentPermissionsWithoutControlPanelCannotAccess {
		property custom.properties = "feature.flag.LPS-166954=true";
		property portal.acceptance = "true";

		task ("Add user and set password") {
			JSONUser.addUser(
				userEmailAddress = "userea@liferay.com",
				userFirstName = "userfn",
				userLastName = "userln",
				userScreenName = "usersn");

			JSONUser.setFirstPassword(
				agreeToTermsAndAnswerReminderQuery = "true",
				requireReset = "false",
				userEmailAddress = "userea@liferay.com");
		}

		task ("Create a new role through Roles Admin Portlet") {
			JSONRole.addRegularRole(roleTitle = "UserWithSegmentPermissionsWithoutControlPanelCannotAccess Role");
		}

		task ("Define permissions without View Control Panel Menu permission") {
			Permissions.definePermissionViaJSONAPI(
				resourceAction = "PERMISSIONS",
				resourceName = "com_liferay_segments_web_internal_portlet_SegmentsPortlet",
				roleTitle = "UserWithSegmentPermissionsWithoutControlPanelCannotAccess Role",
				roleType = "regular");
		}

		task ("When Assign the user with regular role") {
			JSONRole.assignRoleToUser(
				roleTitle = "UserWithSegmentPermissionsWithoutControlPanelCannotAccess Role",
				userEmailAddress = "userea@liferay.com");

			User.logoutAndLoginPG(
				userLoginEmailAddress = "userea@liferay.com",
				userLoginFullName = "userfn userln");
		}

		task ("Assert the Segments portlet is not present and the user cannot access to the Segments page") {
			AssertElementNotPresent(locator1 = "ApplicationsMenu#TOGGLE");
		}
	}

}
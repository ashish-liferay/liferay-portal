@component-name = "portal-wcm"
definition {

	property portal.release = "true";
	property portal.upstream = "true";
	property testray.main.component.name = "Fragments";

	setUp {
		task ("Set up instance and sign in") {
			TestCase.setUpPortalInstance();

			User.firstLoginPG();
		}

		task ("Add a new site") {
			HeadlessSite.addSite(siteName = "Test Site Name");
		}

		task ("Add a company Object with a Long Text field") {
			ObjectAdmin.addObjectViaAPI(
				labelName = "Purchase Order",
				objectName = "PurchaseOrder",
				pluralLabelName = "Purchase Orders");

			ObjectAdmin.addObjectFieldViaAPI(
				fieldBusinessType = "LongText",
				fieldLabelName = "Comments",
				fieldName = "comments",
				fieldType = "Clob",
				isRequired = "false",
				objectName = "PurchaseOrder");

			ObjectAdmin.publishObjectViaAPI(objectName = "PurchaseOrder");
		}
	}

	tearDown {
		var testPortalInstance = PropsUtil.get("test.portal.instance");

		if (${testPortalInstance} == "true") {
			PortalInstances.tearDownCP();
		}
		else {
			JSONGroup.deleteGroupByName(groupName = "Test Site Name");

			ObjectAdmin.deleteObjectViaAPI(objectName = "PurchaseOrder");
		}
	}

	@description = "This is a test for LPS-170206. The page designer can define the number of lines of Textarea fragment."
	@priority = 3
	test DefineNumberOfLines {
		task ("Given a pages designer has a Textarea fragment inside a mapped Form Container") {
			JSONLayout.addPublicLayout(
				groupName = "Test Site Name",
				layoutName = "Test Page Name",
				type = "content");

			ContentPagesNavigator.openEditContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");

			PageEditor.addFragment(
				collectionName = "Form Components",
				fragmentName = "Form Container");

			PageEditor.editFormContainer(
				contentType = "Purchase Order",
				panel = "General");
		}

		task ("When the page designer accesses to the General tab of Textarea fragment") {
			PageEditor.gotoConfiguration(fragmentName = "Textarea");
		}

		task ("Then the page designer sees the default Number of Lines is 5") {
			AssertTextEquals(
				key_text = "Number of Lines",
				locator1 = "TextInput#ANY",
				value1 = 5);
		}

		task ("When the pages designer changes the Number of Lines to 10") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				numberOfLines = 10);
		}

		task ("Then the pages designer sees the rows of Textarea fragment is 10") {
			PageEditor.viewInputStyle(
				numberOfLines = 10,
				type = "textarea");
		}

		task ("When the pages designer changes the Number of Lines to 2") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				numberOfLines = 2);
		}

		task ("Then the pages designer sees the rows of Textarea fragment is 2") {
			PageEditor.viewInputStyle(
				numberOfLines = 2,
				type = "textarea");
		}

		task ("When the pages designer changes the Number of Lines to 0") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				numberOfLines = 0);
		}

		task ("Then the pages designer sees the height of Textarea fragment is 64px") {
			var height = Css.getCssValue(
				attribute = "height",
				element = "//div[contains(@id,'fragment-')]/div[contains(@class,'textarea')]//textarea[contains(@id,'textarea')]");

			TestUtils.assertEquals(
				actual = ${height},
				expected = "64px");
		}

		task ("When the pages designer leaves the Number of Lines empty") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				numberOfLines = "");
		}

		task ("Then the pages designer sees the height of Textarea fragment is 64px") {
			var height = Css.getCssValue(
				attribute = "height",
				element = "//div[contains(@id,'fragment-')]/div[contains(@class,'textarea')]//textarea[contains(@id,'textarea')]");

			TestUtils.assertEquals(
				actual = ${height},
				expected = "64px");
		}
	}

	@description = "This is a test for LPS-170206. The page designer can map Textarea fragment to Long Text field."
	@priority = 4
	test MapTextareaFragmentToLongTextField {
		task ("Given a pages designer has a Form Container on content page") {
			JSONLayout.addPublicLayout(
				groupName = "Test Site Name",
				layoutName = "Test Page Name",
				type = "content");

			ContentPagesNavigator.openEditContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");

			PageEditor.addFragment(
				collectionName = "Form Components",
				fragmentName = "Form Container");
		}

		task ("When the pages designer maps the Form Container to object") {
			PageEditor.editFormContainer(
				contentType = "Purchase Order",
				panel = "General");
		}

		task ("Then the pages designer sees the Textarea fragment mapped to Long Text field") {
			PageEditor.viewInputStyle(
				label = "Comments",
				showLabel = "true",
				type = "textarea");
		}

		task ("When the page designer enables the Show Help Text") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				hideHelpText = "false");
		}

		task ("Then the pages designer sees the Help Text on Textarea fragment") {
			PageEditor.viewInputStyle(
				helpText = "Add your help text here.",
				showHelpText = "true",
				type = "textarea");
		}

		task ("When the pages designer types a new label") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				label = "Additional Comments");
		}

		task ("Then the pages designer sees the new label on Textarea fragment") {
			PageEditor.viewInputStyle(
				label = "Additional Comments",
				showLabel = "true",
				type = "textarea");
		}

		task ("When the page designer types a placeholder") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				placeholder = "Comments Example");
		}

		task ("Then the pages designer sees the placeholder on Textarea fragment") {
			PageEditor.viewInputStyle(
				placeholder = "Comments Example",
				type = "textarea");
		}
	}

	@description = "This is a test for LPS-170206. The page designer can show characters count of Textarea fragment."
	@priority = 3
	test ShowCharactersCount {
		task ("Given a pages designer has a Textarea fragment inside a mapped Form Container") {
			JSONLayout.addPublicLayout(
				groupName = "Test Site Name",
				layoutName = "Test Page Name",
				type = "content");

			ContentPagesNavigator.openEditContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");

			PageEditor.addFragment(
				collectionName = "Form Components",
				fragmentName = "Form Container");

			PageEditor.editFormContainer(
				contentType = "Purchase Order",
				panel = "General");
		}

		task ("When the page designer enables the Show Characters Count of Textarea fragment") {
			PageEditor.editInput(
				fragmentName = "Textarea",
				showCharactersCount = "true");
		}

		task ("Then the page designer sees the characters count shown at the bottom of Textarea fragment") {
			PageEditor.viewInputStyle(
				charactersCount = ": 0 / 65000",
				type = "textarea");

			PageEditor.publish();
		}

		task ("When the page designer navigates to the content page at view mode") {
			ContentPagesNavigator.openViewContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");
		}

		task ("Then the page designer sees the characters count shown at the bottom of Textarea fragment") {
			PageEditor.viewInputStyle(
				charactersCount = ": 0 / 65000",
				type = "textarea");
		}

		task ("When the page designer type text in Textarea fragment") {
			FormFields.editTextArea(
				fieldName = "comments",
				fieldValue = "Liferay");
		}

		task ("Then the page designer sees the characters count shown at the bottom of Textarea fragment") {
			PageEditor.viewInputStyle(
				charactersCount = ": 7 / 65000",
				type = "textarea");
		}
	}

	@description = "This is a test for LPS-170206 and LPS-173849. The user should see alert message when the characters count exceeds the max of Long Text field."
	@priority = 3
	test ViewAlertMessageWhenCharactersCountExceedMaximum {
		task ("Given a pages designer has a Textarea fragment inside a mapped Form Container") {
			task ("Map a Form Container to the object") {
				JSONLayout.addPublicLayout(
					groupName = "Test Site Name",
					layoutName = "Test Page Name",
					type = "content");

				ContentPagesNavigator.openEditContentPage(
					pageName = "Test Page Name",
					siteName = "Test Site Name");

				PageEditor.addFragment(
					collectionName = "Form Components",
					fragmentName = "Form Container");

				PageEditor.editFormContainer(
					contentType = "Purchase Order",
					panel = "General");

				PageEditor.publish();
			}

			task ("Change the Maximum Number of Characters of Comments field to 650") {
				ObjectAdmin.openObjectAdmin();

				ObjectPortlet.selectCustomObject(label = "Purchase Order");

				ObjectAdmin.goToFieldsTab();

				ObjectAdmin.goToFieldsDetails(label = "Comments");

				ObjectField.checkLimitCharacters();

				Type(
					key_text = "Maximum Number of Characters",
					locator1 = "TextInput#ANY",
					value1 = 650);

				ObjectField.save();
			}
		}

		task ("When the page designer types 651 characters exceeding the maximum of Long Text field at view mode") {
			var text = '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus posuere, lectus eget sagittis blandit, justo ex viverra nunc, pellentesque tincidunt lectus velit eu leo. Fusce sollicitudin nisl odio, in hendrerit purus auctor ut. Cras rutrum odio in ante sollicitudin, nec vestibulum dui euismod. Nulla facilisi. Cras hendrerit iaculis metus vitae convallis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam auctor ornare egestas. Etiam sit amet aliquam libero. Donec sed porttitor elit, rhoncus aliquam neque. Nam laoreet feugiat libero id tincidunt. Fusce condimentum euismod ipsum vel viverra.''';

			ContentPagesNavigator.openViewContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");

			FormFields.editTextArea(
				fieldName = "comments",
				fieldValue = ${text});
		}

		task ("Then the page designer sees the alert message shown at the below of Textarea fragment") {
			PageEditor.viewInputStyle(
				charactersCount = "Maximum Number of Characters Exceeded: 651 / 650",
				type = "textarea");
		}

		task ("When the page designer remove one character from Textarea") {
			KeyPress(
				key_text = "comments",
				locator1 = "TextArea#ANY",
				value1 = "\BACK_SPACE");
		}

		task ("Then the page designer cannot see the alert message") {
			AssertTextNotEquals.assertNotPartialText(
				index = 1,
				locator1 = "Fragment#INPUT_LENGTH_INFO",
				type = "textarea",
				value1 = "Maximum Number of Characters Exceeded");
		}

		task ("Given the page designer enables the Show Characters Count of Textarea fragment") {
			ContentPagesNavigator.openEditContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");

			PageEditor.editInput(
				fragmentName = "Textarea",
				showCharactersCount = "true");

			PageEditor.publish();
		}

		task ("When the page designer types 651 characters exceeding the maximum of Long Text field at view mode") {
			ContentPagesNavigator.openViewContentPage(
				pageName = "Test Page Name",
				siteName = "Test Site Name");

			FormFields.editTextArea(
				fieldName = "comments",
				fieldValue = ${text});
		}

		task ("Then the page designer sees the alert message shown at the below of Textarea fragment") {
			PageEditor.viewInputStyle(
				charactersCount = "Maximum Number of Characters Exceeded: 651 / 650",
				type = "textarea");
		}

		task ("When the page designer remove one character from Textarea") {
			KeyPress(
				key_text = "comments",
				locator1 = "TextArea#ANY",
				value1 = "\BACK_SPACE");
		}

		task ("Then the page designer cannot see the alert message") {
			PageEditor.viewInputStyle(
				charactersCount = ": 650 / 650",
				type = "textarea");
		}
	}

}